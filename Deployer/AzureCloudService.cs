using System;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Management.Compute.Models;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace Deployer
{
    internal class AzureCloudService : IDeployTarget
    {
        private CloudStorageAccount storage;
        private CloudBlobClient blobs;
        private CloudBlobContainer container;
        private const double MegaByte = 1 * 1024 * 1024;

        public void PrePackage(dynamic configFile)
        {
            this.storage = CloudStorageAccount.Parse((string)configFile.Azure.Storage);
            this.blobs = this.storage.CreateCloudBlobClient();
            this.container = this.blobs.GetContainerReference((string)configFile.Azure.Container);
        }

        public async Task Deploy(Deployment deployment, dynamic configFile)
        {
            await UploadFile(deployment.PackageFilePath, (string) configFile.Name + ".cspkg", "Uploading package");            

            var certStore = new X509Store(StoreLocation.CurrentUser);
            certStore.Open(OpenFlags.ReadOnly);

            var managementCertificate = certStore.Certificates.Find(X509FindType.FindByThumbprint, (string)configFile.Azure.Certificate, false)[0];

            using (var client = CloudContext.Clients.CreateComputeManagementClient(new CertificateCloudCredentials((string)configFile.Azure.SubscriptionId, managementCertificate)))
            {
                var parameters = new DeploymentCreateParameters()
                {
                    PackageUri = new Uri(this.container.Uri + "/" + (string)configFile.Name + ".cspkg"),
                    Configuration = File.ReadAllText(deployment.ConfigurationFilePath),
                    Label = (string)configFile.Name,
                    StartDeployment = true,
                    Name = (string)configFile.Name,
                    TreatWarningsAsError = false,
                };                

                var result = await client.Deployments.CreateAsync((string)configFile.Azure.Service, DeploymentSlot.Production, parameters);

                if (result.Status == OperationStatus.Failed)
                {
                    throw new Exception(result.Error.Message);
                }

                Console.WriteLine("Deploy to Azure completed");
            }
        }

        private async Task UploadFile(string localFile, string targetFile, string title)
        {
            var cspkg = this.container.GetBlockBlobReference(targetFile);

            using (var fs = File.Open(localFile, FileMode.Open))
            {
                var wrapper = new ProgressStream(fs);
                wrapper.PositionChanged += ConsoleProgress(title);
                await cspkg.UploadFromStreamAsync(wrapper);
            }
        }

        private EventHandler<StreamPositionChangedEventArgs> ConsoleProgress(string title)
        {
            var top = Console.CursorTop;

            return (s, e) =>
            {
                Console.CursorTop = top;
                Console.CursorLeft = 0;

                var positonInMegabytes = e.Position / MegaByte;
                var lengthInMegabytes = e.Length / MegaByte;

                var text = string.Format("{0}: {1,4:P0}{2,8:0.00} of {3:0.00} MB", title, (double)e.Position / (double)e.Length, positonInMegabytes, lengthInMegabytes);

                Console.Write(text.PadRight(Console.WindowWidth));
            };
        }
    }

    public class ProgressStream : Stream
    {
        private readonly Stream innerStream;

        public event EventHandler<StreamPositionChangedEventArgs> PositionChanged;

        public ProgressStream(Stream innerStream)
        {
            this.innerStream = innerStream;
        }

        public override void Flush()
        {
            this.innerStream.Flush();
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            var res = this.innerStream.Seek(offset, origin);

            this.OnPositionChanged();

            return res;
        }

        public override void SetLength(long value)
        {
            this.innerStream.SetLength(value);
            this.OnPositionChanged();
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            var res = this.innerStream.Read(buffer, offset, count);

            this.OnPositionChanged();

            return res;
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            this.innerStream.Write(buffer, offset, count);
            this.OnPositionChanged();
        }

        public override bool CanRead
        {
            get { return this.innerStream.CanRead; }
        }

        public override bool CanSeek
        {
            get { return this.innerStream.CanSeek; }
        }

        public override bool CanWrite
        {
            get { return this.innerStream.CanWrite; }
        }

        public override long Length
        {
            get { return this.innerStream.Length; }
        }

        public override long Position
        {
            get { return this.innerStream.Position; }
            set { this.innerStream.Position = value; this.OnPositionChanged(); }
        }

        private void OnPositionChanged()
        {
            if (this.PositionChanged != null)
            {
                this.PositionChanged(this, new StreamPositionChangedEventArgs(this.innerStream.Position, this.innerStream.Length));
            }
        }
    }

    public class StreamPositionChangedEventArgs : EventArgs
    {
        public long Position { get; private set; }
        public long Length { get; private set; }

        public StreamPositionChangedEventArgs(long position, long length)
        {
            this.Position = position;
            this.Length = length;
        }
    }
}