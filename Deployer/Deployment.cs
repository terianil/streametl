﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Deployer.Schema.Azure;
using Deployer.Schema.Azure.Configuration;
using Deployer.Schema.Azure.Definition;
using Microsoft.Win32;
using SchemaVersion = Deployer.Schema.Azure.Definition.SchemaVersion;

namespace Deployer
{
    public class Deployment
    {
        private readonly Tool csPack;
        private readonly Tool csRun;

        private readonly ServiceDefinition serviceDefinition;
        private readonly List<DeploymentRole> workerRoles;
        private readonly ServiceConfiguration serviceConfiguration;

        public string WorkDirectory { get; private set; }

        public string DefinitionFilePath { get; private set; }

        public string PackageFilePath { get; private set; }

        public string PackageDirectoryPath { get; private set; }

        public string ConfigurationFilePath { get; private set; }

        public Deployment(string workDirectory, string name)
        {
            this.WorkDirectory = workDirectory;

            var sdkPath = PathUtils.FindInstallPath(@"SOFTWARE\Microsoft\Microsoft SDKs\ServiceHosting\v2.4", "InstallPath");
            var emulatorPath = PathUtils.FindInstallPath(@"SOFTWARE\Microsoft\Windows Azure Emulator", "InstallPath");

            this.csPack = new Tool(Path.Combine(sdkPath, "bin", "cspack.exe"));
            this.csRun = new Tool(Path.Combine(emulatorPath, "Emulator", "csrun.exe"));

            this.DefinitionFilePath = Path.Combine(this.WorkDirectory, "CloudService.csdef");
            this.ConfigurationFilePath = Path.Combine(this.WorkDirectory, "CloudService.cscfg");
            this.PackageFilePath = Path.Combine(this.WorkDirectory, "CloudService.cspkg");
            this.PackageDirectoryPath = Path.Combine(this.WorkDirectory, "Package");

            this.workerRoles = new List<DeploymentRole>();

            this.serviceDefinition = new ServiceDefinition
            {
                name = name,
                schemaVersion = SchemaVersion.Item20140624,                
            };

            this.serviceConfiguration = new ServiceConfiguration()
            {
                serviceName = name,
                schemaVersion = Schema.Azure.Configuration.SchemaVersion.Item20140624,
                osVersion = "*",
                osFamily = "4"
            };
        }

        public void Package(PackageKind kind)
        {
            SaveDefinitionFile();
            SaveConfigurationFile();

            RunCsPack(kind);
        }

        private void RunCsPack(PackageKind kind)
        {
            var arguments = new List<string>
            {
                this.DefinitionFilePath,               
            };

            switch (kind)
            {
                case PackageKind.Archive:
                    arguments.Add("/out:" + this.PackageFilePath);
                    break;
                case PackageKind.Directory:
                    arguments.Add("/out:" + this.PackageDirectoryPath);
                    arguments.Add("/copyOnly");
                    break;
            }

            this.csPack.Execute(arguments);
        }

        private void SaveConfigurationFile()
        {
            this.serviceConfiguration.Role = this.workerRoles.Select(x => x.Settings).ToArray();

            var serializer = new XmlSerializer(typeof(ServiceConfiguration));
            using (var output = File.Create(this.ConfigurationFilePath))
            {
                serializer.Serialize(output, this.serviceConfiguration);
            }
        }

        private void SaveDefinitionFile()
        {
            this.serviceDefinition.WorkerRole = this.workerRoles.Select(x => x.Role).ToArray();

            var serializer = new XmlSerializer(typeof(ServiceDefinition));
            using (var output = File.Create(this.DefinitionFilePath))
            {
                serializer.Serialize(output, this.serviceDefinition);
            }
        }

        public void RunOnEmulator()
        {
            this.csRun.Execute(this.PackageDirectoryPath, this.ConfigurationFilePath);
        }

        public DeploymentRole AddWorkerRole(WorkerRole role)
        {
            var deploymentRole = new DeploymentRole(this, role, new RoleSettings
            {
                name = role.name,
                Instances = new TargetSetting
                {
                    count = 1
                }
            });

            this.workerRoles.Add(deploymentRole);

            return deploymentRole;
        }
    }

    public class DeploymentRole
    {
        public Deployment Deployment { get; private set; }
        public WorkerRole Role { get; private set; }
        public RoleSettings Settings { get; private set; }

        public DeploymentRole(Deployment deployment, WorkerRole role, RoleSettings settings)
        {
            this.Deployment = deployment;
            this.Role = role;
            this.Settings = settings;

            role.Endpoints = role.Endpoints ?? new Endpoints();
        }
    }
}
