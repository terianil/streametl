﻿using System;
using System.IO;
using System.Reflection;
using System.Reflection.Emit;
using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Deployer.Schema.Azure;
using Deployer.Schema.Azure.Configuration;
using Deployer.Schema.Azure.Definition;
using Microsoft.WindowsAzure.ServiceRuntime;
using Newtonsoft.Json.Linq;
using TankStationETL;
using Certificate = Deployer.Schema.Azure.Configuration.Certificate;
using ConfigurationSetting = Deployer.Schema.Azure.Definition.ConfigurationSetting;
using StoreLocation = System.Security.Cryptography.X509Certificates.StoreLocation;

namespace Deployer
{
    public static class DeploymentExtensions
    {
        public static DeploymentRole AddClassWorker<TEntryPoint>(this Deployment @this, string roleName)
            where TEntryPoint : RoleEntryPoint
        {
            var roleDirectory = Path.Combine(@this.WorkDirectory, "RoleWrappers", roleName);

            if (!Directory.Exists(roleDirectory))
            {
                Directory.CreateDirectory(roleDirectory);
            }

            var assemblyName = new AssemblyName(roleName);

            var assembly = AppDomain.CurrentDomain.DefineDynamicAssembly(assemblyName, AssemblyBuilderAccess.Save, roleDirectory);

            var module = assembly.DefineDynamicModule(roleName, roleName + ".dll", true);

            var entryPointType = module.DefineType("EntryPoint", TypeAttributes.Public | TypeAttributes.Class, typeof(TEntryPoint));

            entryPointType.CreateType();

            assembly.Save(roleName + ".dll");

            CopyAssembly(typeof(TEntryPoint).Assembly, roleDirectory);

            foreach (var referencedAssembly in typeof(TEntryPoint).Assembly.GetReferencedAssemblies())
            {
                var refAssembly = Assembly.Load(referencedAssembly);
                if (!refAssembly.GlobalAssemblyCache)
                {
                    CopyAssembly(refAssembly, roleDirectory);
                }
            }

            foreach (var attribute in typeof(TEntryPoint).Assembly.GetCustomAttributes<KeepReferenceAttribute>())
            {
                CopyAssembly(attribute.T.Assembly, roleDirectory);
            }

            if (File.Exists(typeof(TEntryPoint).Assembly.Location + ".config"))
            {
                File.Copy(typeof(TEntryPoint).Assembly.Location + ".config", Path.Combine(roleDirectory, roleName + ".dll.config"), true);
            }

            return @this.AddWorkerRole(new WorkerRole
            {
                name = roleName,
                vmsize = "Small",

                Contents = new[]
                {
                    new ContentElement
                    {
                        destination = ".",
                        SourceDirectory = new SourceDirectoryElement()
                        {
                            path = roleDirectory
                        }
                    },                  
                },

                Runtime = new WorkerRuntime
                {
                    EntryPoint = new WorkerEntryPointElement
                    {
                        Item = new NetFxAssemblyEntryPointElement
                        {
                            assemblyName = roleName + ".dll",
                            targetFrameworkVersion = "v4.5"
                        }
                    }
                }
            });
        }

        private static void CopyAssembly(Assembly assembly, string destinationDirectory)
        {
            var mask = Path.ChangeExtension(Path.GetFileName(assembly.Location), ".*");

            var sourceDirectory = Path.GetDirectoryName(assembly.Location);

            var filesToCopy = Directory.EnumerateFiles(sourceDirectory, mask);

            foreach (var file in filesToCopy)
            {
                var target = Path.Combine(destinationDirectory, Path.GetFileName(file));

                File.Copy(Path.Combine(sourceDirectory, file), target, true);
            }
        }

        public static DeploymentRole Setting(this DeploymentRole @this, string name, string value)
        {
            if (!name.StartsWith("Microsoft."))
            {
                @this.Role.ConfigurationSettings = @this.Role.ConfigurationSettings.Plus(new ConfigurationSetting { name = name });
            }

            @this.Settings.ConfigurationSettings = @this.Settings.ConfigurationSettings.Plus(new Schema.Azure.Configuration.ConfigurationSetting
            {
                name = name,
                value = value
            });

            return @this;
        }

        public static DeploymentRole Endpoint(this DeploymentRole @this, string name, Protocol protocol, int port, int? localPort)
        {
            @this.Role.Endpoints.InputEndpoint = @this.Role.Endpoints.InputEndpoint.Plus(new InputEndpoint
            {
                name = name,
                protocol = protocol,
                port = port,
                localPort = localPort.HasValue ? localPort.ToString() : "*"
            });

            return @this;
        }

        public static DeploymentRole Import(this DeploymentRole @this, string name)
        {
            @this.Role.Imports = @this.Role.Imports.Plus(new Import() { moduleName = name });

            return @this;
        }

        public static DeploymentRole Certificate(this DeploymentRole @this, string name, string thumbprint)
        {
            @this.Settings.Certificates = @this.Settings.Certificates.Plus(new Certificate
            {
                name = name,
                thumbprint = thumbprint,
                thumbprintAlgorithm = ThumbprintAlgorithmTypes.sha1
            });

            return @this;
        }

        public static DeploymentRole InstanceInputEndpoint(this DeploymentRole @this, string name, TransportProtocol protocol, int localPort, int publicMinPort, int publicMaxPort)
        {
            @this.Role.Endpoints.InstanceInputEndpoint = @this.Role.Endpoints.InstanceInputEndpoint.Plus(new InstanceInputEndpointElement
            {
                name = name,
                protocol = protocol,
                localPort = localPort,
                AllocatePublicPortFrom = new AllocatePublicPortFromElement
                {
                    FixedPortRange = new PortRange()
                    {
                        min = publicMinPort,
                        max = publicMaxPort
                    }
                }
            });


            return @this;
        }

        public static DeploymentRole RemoteDebugger(this DeploymentRole @this, string certificate)
        {
            var certStore = new X509Store(StoreLocation.CurrentUser);
            certStore.Open(OpenFlags.ReadOnly);

            var cert = certStore.Certificates.Find(X509FindType.FindByThumbprint, certificate, false)[0];

            return @this.Import("RemoteDebuggerConnector")
                .Setting("Microsoft.WindowsAzure.Plugins.RemoteDebugger.Connector.Enabled", "true")
                .Setting("Microsoft.WindowsAzure.Plugins.RemoteDebugger.Connector.Version", "2.4")
                .Setting("Microsoft.WindowsAzure.Plugins.RemoteDebugger.ClientThumbprint", cert.Thumbprint)
                .Setting("Microsoft.WindowsAzure.Plugins.RemoteDebugger.ServerThumbprint", cert.Thumbprint)
                .InstanceInputEndpoint("Microsoft.WindowsAzure.Plugins.RemoteDebugger.Connector", TransportProtocol.tcp, 30398, 30400, 30424)
                .InstanceInputEndpoint("Microsoft.WindowsAzure.Plugins.RemoteDebugger.Forwarder", TransportProtocol.tcp, 31398, 31400, 31424)
                .Certificate("Microsoft.WindowsAzure.Plugins.RemoteDebugger.TransportValidation", cert.Thumbprint);
        }

        public static DeploymentRole Diagnostics(this DeploymentRole @this, string connectionString)
        {
            return @this.Import("Diagnostics")
                .Setting("Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString", connectionString);
        }

        public static DeploymentRole RemoteAccess(this DeploymentRole @this, string userName, string password, string certificate)
        {
            var certStore = new X509Store(StoreLocation.CurrentUser);
            certStore.Open(OpenFlags.ReadOnly);

            var cert = certStore.Certificates.Find(X509FindType.FindByThumbprint, certificate, false)[0];

            var content = new ContentInfo(Encoding.UTF8.GetBytes(password));
            var envelope = new EnvelopedCms(content);

            envelope.Encrypt(new CmsRecipient(cert));
            var passwordBytes = envelope.Encode();

            @this.Import("RemoteAccess")
                .Setting("Microsoft.WindowsAzure.Plugins.RemoteAccess.Enabled", "true")
                .Setting("Microsoft.WindowsAzure.Plugins.RemoteAccess.AccountUsername", userName)
                .Setting("Microsoft.WindowsAzure.Plugins.RemoteAccess.AccountEncryptedPassword", Convert.ToBase64String(passwordBytes))
                .Setting("Microsoft.WindowsAzure.Plugins.RemoteAccess.AccountExpiration", DateTime.Now.AddYears(10).ToString("O"))
                .Certificate("Microsoft.WindowsAzure.Plugins.RemoteAccess.PasswordEncryption", cert.Thumbprint);

            return @this;
        }

        public static DeploymentRole RemoteForwarder(this DeploymentRole @this)
        {
            return @this.Import("RemoteForwarder")
                .Setting("Microsoft.WindowsAzure.Plugins.RemoteForwarder.Enabled", "true");
        }

        public static DeploymentRole ConfigurePlugins(this DeploymentRole @this, JObject plugins)
        {
            foreach (var plugin in plugins)
            {
                var value = (dynamic)plugin.Value;

                switch (plugin.Key)
                {
                    case "Diagnostics":
                        @this.Diagnostics((string)value.ConnectionString);
                        break;
                    case "RemoteAccess":
                        @this.RemoteAccess((string)value.Username, (string)value.Password, (string)value.Certificate);
                        break;
                    case "RemoteForwarder":
                        @this.RemoteForwarder();
                        break;
                    case "RemoteDebugger":
                        @this.RemoteDebugger((string)value.Certificate);
                        break;
                }
            }

            return @this;
        }       
    }
}