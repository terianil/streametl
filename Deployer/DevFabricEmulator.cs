using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.ServiceHosting.Tools.DevelopmentFabric;

namespace Deployer
{
    internal class DevFabricEmulator : IDeployTarget
    {
        private DevFabric fabric;

        public void PrePackage(dynamic configFile)
        {
            var emulatorDirectory = PathUtils.FindInstallPath(@"SOFTWARE\Microsoft\Windows Azure Emulator", "InstallPath");

            Environment.SetEnvironmentVariable("WindowsAzureEmulatorInstallPath", emulatorDirectory);

            this.fabric = new DevFabric(true);

            foreach (var d in this.fabric.GetDeployments())
            {
                d.Stop();
                d.WaitForState(RoleInstanceStatus.Stopped, TimeSpan.FromMinutes(5));
                d.Delete();
            }
        }

        public Task Deploy(Deployment deployment, dynamic configFile)
        {
            fabric.EnsureDeveloperFabricRunning();
            fabric.EnsureUIRunning();

            var options = new DeploymentOptions(true, 0, true, true, true);

            var fabricDeployment = fabric.CreateDeployment(deployment.PackageDirectoryPath, deployment.ConfigurationFilePath, options);                        
            fabricDeployment.Start();

            return Task.FromResult(0);
        }
    }
}