﻿using System.Linq;

namespace Deployer
{
    static class Extensions
    {
        public static T[] Plus<T>(this T[] @this, T newValue)
        {
            if (@this == null)
            {
                return new[] {newValue};
            }

            return @this.Union(new[] {newValue}).ToArray();
        }
    }
}