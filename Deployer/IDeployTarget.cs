using System.Threading.Tasks;

namespace Deployer
{
    public interface IDeployTarget
    {
        Task Deploy(Deployment deployment, dynamic configFile);
        void PrePackage(dynamic configFile);
    }
}