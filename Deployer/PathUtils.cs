using Microsoft.Win32;

namespace Deployer
{
    static internal class PathUtils
    {
        public static string FindInstallPath(string keyName, string valueName)
        {
            using (var key = Registry.LocalMachine.OpenSubKey(keyName))
            {
                return (string)key.GetValue(valueName);
            }
        }
    }
}