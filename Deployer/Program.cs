﻿using System;
using System.Diagnostics;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Deployer.Schema.Azure;
using Deployer.Schema.Azure.Definition;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using TankStationETL;
using Task = System.Threading.Tasks.Task;

namespace Deployer
{
    class Program
    {
        static void Main(string[] args)
        {
            MainAsync(args).Wait();
        }

        static async Task MainAsync(string[] args)
        {
            var configFile = (dynamic)JObject.Parse(File.ReadAllText(args[0], Encoding.UTF8));

            var deployment = new StreamEtlService(args[1], (string) configFile.Name)
            {
                LogUri = (string) configFile.LogUri,
                Plugins = (JObject) configFile.Plugins,
                RabbitMqUri = (string)configFile.RabbitMq
            };

            deployment.AddRemoteBufferFramework<NozzleRbf>("nozzle", "rif_nozzle_input", "rbf_nozzle_history")
                .Endpoint("web", Protocol.http, 9999, null)                
                ;

            deployment.AddRemoteBufferFramework<TankRbf>("tank", "rif_tank_input", "rbf_tank_history")
                .Endpoint("web", Protocol.http, 9998, null);

            deployment.AddRemoteIntegratorFramework<NozzleRif>("nozzle", "etl_input");

            deployment.AddRemoteIntegratorFramework<TankRif>("tank", "etl_input");

            deployment.AddEtl<TankStationEtlRt>("etl", "fti_input");

            deployment.AddFti<FtiMongoDb>("fti", (string)configFile.MongoDb);
            
            IDeployTarget target = null;

            switch ((string)configFile.DeployTarget)
            {
                case "emulator":
                    target = new DevFabricEmulator();
                    break;
                case "azure":
                    target = new AzureCloudService();
                    break;
                case "dummy":
                    break;
                default:
                    throw new InvalidOperationException("Unknown deploy target");
            }

            try
            {
                if (target != null)
                {
                    target.PrePackage(configFile);
                
                    Console.WriteLine("Packaging");

                    deployment.Package(PackageKind.Directory);
                    deployment.Package(PackageKind.Archive);

                    Console.WriteLine("Deploying");

                    await target.Deploy(deployment, configFile);   
                }

                deployment.PostDeployment();
            }
            catch (ToolExecutionFailed e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StdOut);
                Console.WriteLine(e.StdErr);
            }
        }
    }
}
