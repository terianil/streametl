using Newtonsoft.Json.Linq;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using TankStationETL;

namespace Deployer
{
    public class StreamEtlService : Deployment
    {
        public StreamEtlService(string workDirectory, string name)
            : base(workDirectory, name)
        {
            this.Queues = new HashSet<string>();
        }

        public string LogUri { get; set; }
        public string RabbitMqUri { get; set; }
        public JObject Plugins { get; set; }
        public HashSet<string> Queues { get; set; }

        public DeploymentRole AddRemoteBufferFramework<T>(string name, params string[] outbbound)
            where T : RemoteBufferFramework
        {
            var inputQueue = string.Format("rbf_{0}_input", name);
            this.Queues.Add(inputQueue);
            this.Queues.UnionWith(outbbound);

            return this.AddClassWorker<T>("Rbf." + name)
                .Setting("logUri", this.LogUri)
                .Setting("rabbitMq", this.RabbitMqUri)
                .Setting("name", name)
                .Setting("outbound", string.Join(",", outbbound))
                .Setting("rbfInputName", inputQueue)
                .ConfigurePlugins(this.Plugins);
        }

        public DeploymentRole AddRemoteIntegratorFramework<T>(string name, string outbbound)
            where T : RemoteIntegratorFramework
        {
            var inputQueue = string.Format("rif_{0}_input", name);
            this.Queues.Add(inputQueue);
            this.Queues.Add(outbbound);

            return this.AddClassWorker<T>("Rif." + name)
                .Setting("logUri", this.LogUri)
                .Setting("rabbitMq", this.RabbitMqUri)
                .Setting("name", name)
                .Setting("outbound", outbbound)
                .Setting("rifInputName", inputQueue)
                .ConfigurePlugins(this.Plugins);
        }

        public DeploymentRole AddEtl<T>(string name, string ftiInput)
            where T : EtlBase
        {
            var inputQueue = string.Format("{0}_input", name);
            this.Queues.Add(inputQueue);
            this.Queues.Add(ftiInput);

            return this.AddClassWorker<T>("Etl." + name)
                .Setting("logUri", this.LogUri)
                .Setting("rabbitMq", this.RabbitMqUri)
                .Setting("name", name)
                .Setting("outbound", ftiInput)
                .Setting("etlInputName", inputQueue)
                .ConfigurePlugins(this.Plugins);
        }

        public DeploymentRole AddFti<T>(string name, string mongoDb)
            where T : FtiBase
        {
            var inputQueue = string.Format("{0}_input", name);

            return this.AddClassWorker<T>("Etl." + name)
                .Setting("logUri", this.LogUri)
                .Setting("rabbitMq", this.RabbitMqUri)
                .Setting("name", name)
                .Setting("ftiInputName", inputQueue)
                .Setting("mongoDb", mongoDb)
                .ConfigurePlugins(this.Plugins);
        }

        public void PostDeployment()
        {
            var connectionFactory = new ConnectionFactory();
            connectionFactory.Endpoint = new AmqpTcpEndpoint(new Uri(RabbitMqUri));

            using (var connection = connectionFactory.CreateConnection())
            {
                var model = connection.CreateModel();

                foreach (var queue in this.Queues)
                {
                    model.ExchangeDeclare(queue, "direct");
                    model.QueueDeclare(queue, true, false, false, new Dictionary<string, object>());

                    model.QueueBind(queue, queue, string.Empty);
                }
            }
        }
    }
}