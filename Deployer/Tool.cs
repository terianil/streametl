﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Deployer
{
    class Tool
    {
        public string ExePath { get; private set; }

        public Tool(string exePath)
        {
            this.ExePath = exePath;
        }

        public void Execute(params string[] arguments)
        {
            this.Execute((IEnumerable<string>)arguments);
        }

        public void Execute(IEnumerable<string> arguments)
        {
            var commandArguments = string.Join(" ", arguments.Select(x => "\"" + x + "\""));

            var startInfo = new ProcessStartInfo(this.ExePath, commandArguments)
            {
                CreateNoWindow = false,
                UseShellExecute = false,

                RedirectStandardOutput = true,
                RedirectStandardError = true
            };

            var process = Process.Start(startInfo);

            process.WaitForExit();

            if (process.ExitCode != 0)
            {
                var stdOut = process.StandardOutput.ReadToEnd();
                var stdErr = process.StandardError.ReadToEnd();

                throw new ToolExecutionFailed(Path.GetFileName(this.ExePath), stdOut, stdErr);
            }
        }
    }

    internal class ToolExecutionFailed : Exception
    {
        public string StdOut { get; private set; }
        public string StdErr { get; private set; }

        public ToolExecutionFailed(string toolName, string stdOut, string stdErr)
            : base("Tool " + toolName + " failed")
        {
            this.StdOut = stdOut;
            this.StdErr = stdErr;
        }
    }
}
