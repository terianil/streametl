﻿using Autofac;
using Nancy;
using Nancy.Bootstrappers.Autofac;

namespace Generator
{
    /// <summary>
    /// Autofac Nancy setup.
    /// </summary>
    public class Bootstraper : AutofacNancyBootstrapper
    {
        private readonly ILifetimeScope container;

        public Bootstraper(ILifetimeScope container)
        {
            this.container = container;
        }

        protected override ILifetimeScope GetApplicationContainer()
        {
            return this.container;
        }

        protected override void ConfigureApplicationContainer(ILifetimeScope existingContainer)
        {
            base.ConfigureApplicationContainer(existingContainer);

            StaticConfiguration.DisableErrorTraces = false;
        }
    }
}