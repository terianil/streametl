﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using Generator.Proxies;
using Generator.Topology;
using Nancy;

namespace Generator.DI
{
    class GeneratorModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<GeneratorApp>().AsSelf();            
            builder.RegisterType<PouringDirector>().AsSelf();
            builder.RegisterType<OutputStreamer>().AsSelf();

            builder.Register(ctx => ctx.ComponentRegistry);

            builder.RegisterType<HomeModule>().As<INancyModule>();

            builder.RegisterType<PutInvalidValuesProxy>()
                .Keyed<IHttpProxy>("nozzle");

            builder.RegisterType<NormalOperationProxy>()
                .Keyed<IHttpProxy>("tank");
        }
    }
}
