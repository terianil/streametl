﻿using System;
using System.Collections.Generic;
using Autofac;
using Autofac.Core;
using Generator.Topology;
using Microsoft.Owin.Hosting;
using Nancy.Owin;
using Owin;

namespace Generator
{
    /// <summary>
    /// Genarator base application.
    /// </summary>
    public class GeneratorApp
    {
        private readonly ILifetimeScope container;
        private readonly IComponentRegistry registry;

        private IDisposable webHost;
        private OutputStreamer output;
        private string tankOutput;
        private string nozzleOutput;

        public GeneratorApp(ILifetimeScope container, IComponentRegistry registry)
        {
            this.container = container;
            this.registry = registry;
        }

        /// <summary>
        /// Starts generator web application.
        /// </summary>
        /// <param name="topologyDescription"></param>
        public void Start(Description topologyDescription)
        {            
            var tankStation = new TankStation(topologyDescription);

            var builder = new ContainerBuilder();
            builder.RegisterInstance(tankStation);
            builder.Update(this.registry);

            var url = "http://127.0.0.1:9999/generator";
            this.webHost = WebApp.Start(url, WebAppBuilder);

            Console.WriteLine("Web endpoint: {0}", url);

            this.output = this.container.Resolve<OutputStreamer>();
            this.output.Start(this.tankOutput, this.nozzleOutput);
        }

        private void WebAppBuilder(IAppBuilder app)
        {
            app.UseWelcomePage("/welcome");

            app.UseErrorPage();

            app.UseNancy(new NancyOptions()
            {
                Bootstrapper = new Bootstraper(this.container)
            });           
        }

        public void Stop()
        {
            this.output.Stop();
            this.webHost.Dispose();
        }

        /// <summary>
        /// Setups nozzle and tank tuple destinations.
        /// </summary>
        /// <param name="tankOutput">Tank output.</param>
        /// <param name="nozzleOutput">Nozzle output.</param>
        public void SetOutput(string tankOutput, string nozzleOutput)
        {
            this.tankOutput = tankOutput;
            this.nozzleOutput = nozzleOutput;
        }
    }
}