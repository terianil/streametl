using System.Linq;
using Generator.Topology;
using Nancy;

namespace Generator
{
    /// <summary>
    /// Generator rest api.
    /// </summary>
    class HomeModule : NancyModule
    {
        /// <summary>
        /// Topology view and pour tank nozzle action.
        /// </summary>
        /// <param name="tankStation"></param>
        /// <param name="pouring"></param>
        public HomeModule(TankStation tankStation, PouringDirector pouring)
        {
            Get["/topology"] = _ => new
            {
                CurrentTime = tankStation.Timestamp,
                TimeSinceStart = tankStation.VirtualTimeSinceStart,
                Tanks = from t in tankStation.Tanks
                        select new
                        {
                            Index = t.TankIndex,
                            Capacity = t.Capacity,
                            FuelVolume = t.FuelVolume,
                            WaterVolume = t.WaterVolume,
                            LiquidVolume = t.LiquidVolume,
                            Temperature = t.Temperature,
                            Nozzles = from n in t.Nozzles
                                      select new
                                      {
                                          Index = n.Index,
                                          TotalVolumePumped = n.TotalVolumePumped,
                                          CalbrationFactor = n.CalibrationFactor
                                      }
                        }
            };

            Post["/tank/{tank}/nozzle/{nozzle}/pour"] = _ =>
            {
                pouring.Initiate(_.tank, _.nozzle, Request.Query.volume);

                return "OK";
            };
        }
    }
}