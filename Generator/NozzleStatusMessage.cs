﻿using System;
using System.Text;

namespace Generator
{
    /// <summary>
    /// Representation of nozzle tupple/
    /// </summary>
    public class NozzleStatusMessage
    {
        public int TankId { get; set; }
        public int NozzleId { get; set; }
        public DateTime Timestamp { get; set; }
        public double CalibrationFactor { get; set; }
        public double TotalVolumePumped { get; set; }
        public string SendTime { get; set; }

        public override string ToString()
        {
            var builder = new StringBuilder();
            builder.Append("{ TankId = ");
            builder.Append(this.TankId);
            builder.Append(", NozzleId = ");
            builder.Append(this.NozzleId);
            builder.Append(", Timestamp = ");
            builder.Append(this.Timestamp);
            builder.Append(", CalibrationFactor = ");
            builder.Append(this.CalibrationFactor);
            builder.Append(", TotalVolumePumped = ");
            builder.Append(this.TotalVolumePumped);
            builder.Append(" }");
            return builder.ToString();
        }
    }
}