﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Autofac.Features.Indexed;
using Generator.Proxies;
using Generator.Topology;
using NLog;

namespace Generator
{
    /// <summary>
    /// Generator output class. Responsible for sending tuples to ETL process.
    /// </summary>
    class OutputStreamer
    {
        private readonly TankStation tankStation;
        private readonly IIndex<string, IHttpProxy> proxySelector;
        private readonly CancellationTokenSource cancelation;

        private static readonly Logger Log = LogManager.GetLogger("OutputStream");
        private HttpClient tankOutput;
        private HttpClient nozzleOutput;

        private IHttpProxy tankProxy;
        private IHttpProxy nozzleProxy;

        public OutputStreamer(TankStation tankStation, IIndex<string, IHttpProxy> proxySelector)
        {
            this.tankStation = tankStation;
            this.proxySelector = proxySelector;
            this.cancelation = new CancellationTokenSource();
        }

        /// <summary>
        /// Setups http proxy for nozzle and tank output.
        /// </summary>
        /// <param name="tankOutputUri">Tank output url.</param>
        /// <param name="nozzleOutputUri">Nozzle output url.</param>
        public void Start(string tankOutputUri, string nozzleOutputUri)
        {
            this.tankOutput = new HttpClient() { BaseAddress = new Uri(tankOutputUri) };
            this.nozzleOutput = new HttpClient() { BaseAddress = new Uri(nozzleOutputUri) };

            Console.WriteLine("tank uri:" + tankOutput.BaseAddress);
            Console.WriteLine("nozzle uri:" + nozzleOutput.BaseAddress);

            this.tankProxy = this.proxySelector["tank"];
            this.nozzleProxy = this.proxySelector["nozzle"];

            Task.Run(() => StreamData(cancelation.Token), cancelation.Token);
        }

        /// <summary>
        /// Sends data for each tank and nozzle.
        /// </summary>
        /// <param name="token"></param>
        private void StreamData(CancellationToken token)
        {
            while (!token.IsCancellationRequested)
            {
                foreach (var tank in this.tankStation.Tanks)
                {
                    this.SendTankOutput(tank);

                    foreach (var nozzle in tank.Nozzles)
                    {
                        Thread.Sleep(TimeSpan.FromMilliseconds(300));

                        this.SendNozzleOutput(tank.TankIndex, nozzle);
                    }
                }
            }
        }

        private void SendNozzleOutput(int tankIndex, Nozzle nozzle)
        {
            var msg = new NozzleStatusMessage
            {
                TankId = tankIndex,
                NozzleId = nozzle.Index,
                Timestamp = this.tankStation.Timestamp,
                CalibrationFactor = nozzle.CalibrationFactor,
                TotalVolumePumped = nozzle.TotalVolumePumped,
                SendTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss,fff")
            };

            try
            {
                var result = this.nozzleProxy.Send(this.nozzleOutput, msg).Result;
                result
                    .EnsureSuccessStatusCode();
            }
            catch (Exception e)
            {
                Log.Error("Error sending tuples to output: " + this.nozzleOutput.BaseAddress, e);
            }
        }

        private void SendTankOutput(Tank tank)
        {
            var msg = new
            {
                TankId = tank.TankIndex,
                Timestamp = this.tankStation.Timestamp,
                VolumeBrutto = tank.FuelVolume,
                WaterVolume = tank.WaterVolume,
                Temperatue = tank.Temperature,
                SendTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss,fff")
            };

            try
            {
                var result = this.tankProxy.Send(this.tankOutput, msg)
                    .Result;
                result
                    .EnsureSuccessStatusCode();
            }
            catch (Exception e)
            {
                Log.Error("Error sending tuples to output: " + this.tankOutput.BaseAddress, e);
            }
        }

        public void Stop()
        {
            this.cancelation.Cancel();
        }
    }
}
