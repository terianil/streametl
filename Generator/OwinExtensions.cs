﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;

namespace Generator
{
    /// <summary>
    /// Owin extension methods.
    /// </summary>
    internal static class OwinExtensions
    {
        public static IAppBuilder Run(this IAppBuilder app, string path, Func<IOwinContext, Task> action)
        {
            return app.Map(path, a => a.Run(action));            
        }
    }
}