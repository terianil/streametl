﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using Generator.DI;
using Generator.Topology;
using Newtonsoft.Json;
using NLog;

namespace Generator
{
    class Program
    {
        private static readonly Logger Log = LogManager.GetLogger("StartUp");

        static void Main(string[] args)
        {
            Log.Info("Starting up Data Generator");

            var topologyDescription = JsonConvert.DeserializeObject<Description>(File.ReadAllText(args[0]));

            var container = BuildContainer();

            var app = container.Resolve<GeneratorApp>();
            app.SetOutput(args[1], args[2]);
            
            app.Start(topologyDescription);

            Console.ReadLine();

            Log.Info("Shutting down...");

            app.Stop();
        }

        private static IContainer BuildContainer()
        {
            var containerBuilder = new ContainerBuilder();

            containerBuilder.RegisterModule<GeneratorModule>();

            return containerBuilder.Build();
        }
    }
}
