using System.Net.Http;
using System.Threading.Tasks;

namespace Generator.Proxies
{
    /// <summary>
    /// Interface for proxies that 'break' messages
    /// </summary>
    public interface IHttpProxy
    {
        Task<HttpResponseMessage> Send<T>(HttpClient httpClient, T message);
    }
}