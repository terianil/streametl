using System.Net.Http;
using System.Threading.Tasks;

namespace Generator.Proxies
{
    /// <summary>
    /// Proxy that does nothing to messages
    /// </summary>
    public class NormalOperationProxy : IHttpProxy
    {
        public async Task<HttpResponseMessage> Send<T>(HttpClient httpClient, T message)
        {
            return await httpClient.PostAsJsonAsync("input", message);
        }
    }
}