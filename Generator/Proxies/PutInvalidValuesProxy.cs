﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using NLog;

namespace Generator.Proxies
{
    /// <summary>
    /// Proxy that puts random values to NozzleStatusMessage
    /// Multiplies TotalValuePumped with random value from range (-1,1)
    /// </summary>
    public class PutInvalidValuesProxy : IHttpProxy
    {
        private static readonly Logger Log = LogManager.GetLogger("InvalidValuesProxy");
        private Random r;

        public PutInvalidValuesProxy()
        {
            this.r = new Random(666);
        }

        public async Task<HttpResponseMessage> Send<T>(HttpClient httpClient, T message)
        {
            if (message is NozzleStatusMessage)
            {
                MessWithNozzleStatusMessage(message as NozzleStatusMessage);
            }

            return await httpClient.PostAsJsonAsync("input", message);
        }

        private void MessWithNozzleStatusMessage(NozzleStatusMessage message)
        {
            var old = message.TotalVolumePumped;
            message.TotalVolumePumped *= r.Next(-1, 1);

            Log.Info("Changed TotalVolumePumped from {0} to {1} in {2}", old, message.TotalVolumePumped, message);
        }
    }
}