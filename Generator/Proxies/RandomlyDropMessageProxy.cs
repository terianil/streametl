﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using NLog;

namespace Generator.Proxies
{
    /// <summary>
    /// Randomly drops messages
    /// 30% of messages will reach their destination
    /// </summary>
    public class RandomlyDropMessageProxy : IHttpProxy
    {
        private static readonly Logger Log = LogManager.GetLogger("OutputProxy");

        private readonly Random r;

        public RandomlyDropMessageProxy()
        {
            this.r = new Random(666);
        }

        public async Task<HttpResponseMessage> Send<T>(HttpClient httpClient, T message)
        {
            if (this.r.NextDouble() <= 0.3)
            {
                Log.Trace("Sent message {0}", message);

                return await httpClient.PostAsJsonAsync("input", message);                
            }
            else
            {
                Log.Info("Dropped message {0}", message);

                return new HttpResponseMessage(HttpStatusCode.OK);
            }
        }
    }
}