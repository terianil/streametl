﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using NLog;

namespace Generator.Proxies
{
    /// <summary>
    /// Proxy that shuffles messages
    /// Last 3 messages are buffered, than send in random order
    /// </summary>
    class ShuffleProxy : IHttpProxy
    {
        private List<object> messages;
        private const int MessagesLimit = 3;
        private Random r = new Random(667);
        private static readonly Logger log = LogManager.GetLogger("ShuffleProxy");

        public ShuffleProxy()
        {
            messages = new List<object>();
        }

        public async Task<HttpResponseMessage> Send<T>(HttpClient httpClient, T message)
        {
            messages.Add(message);

            if (messages.Count > MessagesLimit)
            {
                var index = r.Next(0, MessagesLimit + 1);
                var toSend = (T) messages[index];
                messages.RemoveAt(index);

                log.Info("Sending shuffled message {0}", toSend);

                return await httpClient.PostAsJsonAsync<T>("input", toSend);
            }

            return new HttpResponseMessage(HttpStatusCode.OK);
        }
    }
}
