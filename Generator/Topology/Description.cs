﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generator.Topology
{
    /// <summary>
    /// Topology description
    /// </summary>
    public class Description
    {        
        public TimeSpan DayLength { get; set; }
        
        public TankDescription[] Tanks { get; set; }
    }

    public class TankDescription
    {
        public double Capacity { get; set; }
        
        public double TemperatureBase { get; set; }
        public double TemperatureAmplitude { get; set; }
        
        public double InitialFuel { get; set; }

        public NozzleDescription[] Nozzles { get; set; }
    }

    public class NozzleDescription
    {
        public double Factor { get; set; }
    }
}
