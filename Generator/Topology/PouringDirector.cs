﻿using System;
using System.Threading;
using System.Threading.Tasks;
using NLog;

namespace Generator.Topology
{   
    /// <summary>
    /// Controls the process of pouring from nozzle
    /// Pouring is done by pouring small volumes (quantums) than waiting for some time and pouring next quantum
    /// That gives almost continous process
    /// </summary>
    class PouringDirector
    {
        private static readonly Logger Log = LogManager.GetLogger("PouringDirector");
        private readonly TankStation station;

        public PouringDirector(TankStation station)
        {
            this.station = station;
        }

        public void Initiate(int tankId, int nozzleId, double amount)
        {
            Log.Info("Starting pour on {0}/{1} amount: {2:0.00}", tankId, nozzleId, amount);

            Task.Run(() => Pour(tankId, nozzleId, amount));
        }

        private void Pour(int tankId, int nozzleId, double amount)
        {
            var log = LogManager.GetLogger(string.Format("Pour.Tank{0}Nozzle{1}", tankId, nozzleId));

            var remainingAmount = amount;

            while (remainingAmount > 0)
            {
                log.Trace("Remaining volume to pour {0:0.00}", remainingAmount);

                var quantum = 0.3;
                var timePerQuantum = TimeSpan.FromMilliseconds(200);

                this.station.Tanks[tankId].Nozzles[nozzleId].Pour(quantum);
                
                Thread.Sleep(timePerQuantum);
                remainingAmount -= quantum;
            }

            log.Info("Poured {0:0.00} from tank {1} nozzle {2}", amount, tankId, nozzleId);
        }
    }
}
