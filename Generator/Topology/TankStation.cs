﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Generator.Topology
{
    /// <summary>
    /// Represents model of Tank Station with tanks and nozzles. Also controls simulation time
    /// </summary>
    class TankStation
    {
        private readonly DateTime startTimestamp;

        private readonly double timeScaleFactor;

        public Tank[] Tanks { get; private set; }

        public DateTime Timestamp
        {
            get
            {
                var timeFromStart = DateTime.Now - startTimestamp;

                var scaledTimeFromStart = TimeSpan.FromSeconds(timeFromStart.TotalSeconds * this.timeScaleFactor);

                return startTimestamp + scaledTimeFromStart;
            }
        }

        public TimeSpan VirtualTimeSinceStart
        {
            get { return this.Timestamp - this.startTimestamp; }
        }

        public TankStation(Description description)
        {
            this.startTimestamp = DateTime.Now;

            this.timeScaleFactor = TimeSpan.FromDays(1).TotalSeconds / description.DayLength.TotalSeconds;

            this.Tanks = description.Tanks
                .Select((d, i) => new Tank(this, i, d))
                .ToArray();
        }
    }

    internal class Tank
    {
        private readonly TankStation station;
        private readonly double temperatureBase;
        private readonly double temperatureAmplitude;

        public int TankIndex { get; private set; }

        public Nozzle[] Nozzles { get; private set; }

        public double Capacity { get; set; }
        public double FuelVolume { get; set; }

        public double Temperature
        {
            get
            {
                var dayProgress = station.VirtualTimeSinceStart.TotalSeconds / TimeSpan.FromDays(1).TotalSeconds;

                return this.temperatureBase + this.temperatureAmplitude * Math.Sin(2 * Math.PI * dayProgress);
            }
        }

        public double WaterVolume
        {
            get { return 100; }
        }

        public double LiquidVolume
        {
            get { return this.WaterVolume + this.FuelVolume; }
        }

        public Tank(TankStation station, int tankIndex, TankDescription description)
        {
            this.station = station;
            this.TankIndex = tankIndex;

            this.Capacity = description.Capacity;
            this.FuelVolume = description.InitialFuel;

            this.temperatureBase = description.TemperatureBase;
            this.temperatureAmplitude = description.TemperatureAmplitude;

            this.Nozzles = description.Nozzles
                .Select((d, i) => new Nozzle(this, i, d))
                .ToArray();
        }

        public void Pour(double volume)
        {
            lock (this)
            {
                this.FuelVolume -= volume;
            }
        }
    }

    internal class Nozzle
    {
        private readonly Tank tank;

        public int Index { get; private set; }

        public double TotalVolumePumped { get; private set; }

        public double CalibrationFactor { get; private set; }

        public Nozzle(Tank tank, int index, NozzleDescription description)
        {
            this.tank = tank;
            this.Index = index;
            this.TotalVolumePumped = 0;
            this.CalibrationFactor = description.Factor;
        }

        public void Pour(double volume)
        {
            lock (this)
            {
                this.tank.Pour(volume / this.CalibrationFactor);
                this.TotalVolumePumped += volume;
            }
        }
    }
}
