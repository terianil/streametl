﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(LogWatcher.Startup))]

namespace LogWatcher
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseErrorPage();
            app.UseWelcomePage("/welcome");
            app.Map("/clear", x => x.Run(i => {
                LogBuffer.Clear();
                i.Response.Redirect(i.Request.Uri.ToString().Replace("/clear", string.Empty));
                return Task.FromResult(0);
            }));

            app.MapSignalR();
        }
    }

    public static class LogBuffer
    {
        private const int BufferSize = 1000;

        private static readonly List<LogEntry> Buffer = new List<LogEntry>();
        private static readonly object SyncRoot = new object();

        public static void Append(LogEntry entry)
        {
            Buffer.Add(entry);

            if (Buffer.Count > BufferSize)
            {
                lock (SyncRoot)
                {
                    while (Buffer.Count > BufferSize)
                    {
                        Buffer.RemoveAt(0);
                    }
                }
            }
        }

        public static void Clear()
        {
            Buffer.Clear();
        }

        public static IEnumerable<LogEntry> Dump()
        {
            lock (SyncRoot)
            {
                return new List<LogEntry>(Buffer);
            }
        }
    }

    public class LogHub : Hub
    {
        public void Watch()
        {
            this.Groups.Add(this.Context.ConnectionId, "Watchers");

            var buffer = LogBuffer.Dump();

            foreach (var logEntry in buffer)
            {
                this.Send(this.Clients.Caller, logEntry);
            }
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            this.Groups.Remove(this.Context.ConnectionId, "Watchers");

            return base.OnDisconnected(stopCalled);
        }

        public void Log(string workerName, string logLevel, string logger, DateTime at, string message)
        {
            var entry = new LogEntry
            {
                WorkerName = workerName,
                LogLevel = logLevel,
                Logger = logger,
                At = at,
                Message = message
            };

            LogBuffer.Append(entry);

            this.Send(this.Clients.Group("Watchers"), entry);
        }

        private void Send(dynamic target, LogEntry entry)
        {
            target.NewMessage(entry.WorkerName, entry.LogLevel, entry.Logger, entry.At, entry.Message);
        }
    }

    public class LogEntry
    {
        public string WorkerName { get; set; }
        public string LogLevel { get; set; }
        public string Logger { get; set; }
        public DateTime At { get; set; }
        public string Message { get; set; }
    }
}
