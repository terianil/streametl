﻿using System.Collections.Generic;

namespace TankStationETL.Common.Dtos
{
    public class CreateNodeDto
    {
        public CreateNodeDto(string target, NodeType nodeType, List<string> endpointList)
        {
            Target = target;
            NodeType = nodeType;
            EndpointList = endpointList;
        }

        public string Target { get; set; }
        public NodeType NodeType { get; set; }
        public List<string> EndpointList { get; set; }
    }

    public enum NodeType
    {
        None,
        Rbf,
        Rif,
        Etlrt,
        Fti
    }
}