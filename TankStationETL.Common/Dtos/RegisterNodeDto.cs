namespace TankStationETL.Common.Dtos
{
    public class RegisterNodeDto
    {
        public string Name { get; set; }
    }
}
