using System.Collections.Generic;

namespace TankStationETL.Common.Dtos
{
    public class SetEndpointsDto
    {
        public List<string> EndpointList { get; set; }

        public SetEndpointsDto(List<string> endpointList)
        {
            EndpointList = endpointList;
        }
    }
}