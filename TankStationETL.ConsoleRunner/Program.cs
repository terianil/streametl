﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Akka.Actor;
using Akka.Routing;
using TankStationETL.DTO;
using TankStationETL.Model;
using TankStationETL.Supervisors;

namespace TankStationETL.ConsoleRunner {
    class Program {
        static void Main(string[] args)
        {
            var system = ActorSystem.Create("system");

            /*var rbfSup = system.ActorOf<RbfsSupervisor>("rbfs");
            var rifSup = system.ActorOf<RifsSupervisor>("rifs");
            var etlSup = system.ActorOf<EtlSupervisor>("etls");
            var ftiSup = system.ActorOf<FtiSupervisor>("ftis");

            rbfSup.Tell(new CreateRbfsRequest(){ NumerOfRbfs = 3 });
            rifSup.Tell(new CreateRifsRequest(){ NumerOfRifs = 3 });
            etlSup.Tell(new CreateEtlsRequest(){ NumerOfEtls = 2 });
            ftiSup.Tell(new CreateFtisRequest(){ NumerOfFtis = 1 });*/

            system.ActorOf<Rbf>("rbf");

            Thread.Sleep(-1);
        }
    }
}
