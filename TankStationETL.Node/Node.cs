﻿using System;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Akka.Actor;
using TankStationETL.Common.Dtos;
using TankStationETL.Model;

namespace TankStationETL.Node
{
    public class Node : TypedActor, IHandle<CreateNodeDto>, IHandle<PingDto>
    {
        public void Handle(CreateNodeDto message)
        {
            IActorRef nodeActor = CreateNode(message.NodeType);
            nodeActor.Tell(new SetEndpointsDto(message.EndpointList));
        }

        private IActorRef CreateNode(NodeType nodeType)
        {
            switch (nodeType)
            {
                case NodeType.Rbf:
                    return Context.ActorOf<Rbf>("rbf");
                case NodeType.Rif:
                    return Context.ActorOf<Rif>("rif");
                case NodeType.Etlrt:
                    return Context.ActorOf<EtlRt>("etl");
                case NodeType.Fti:
                    return Context.ActorOf<FtiMongoDb>("fti");
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public void Handle(PingDto message)
        {
            this.Sender.Tell(new PongDto());
        }

        public override void AroundPreStart()
        {
            var greeter = Context.ActorSelection(ConfigurationManager.AppSettings.Get("SupervisorActorAddress"));
            greeter.Tell(new RegisterNodeDto() { Name = System.Net.Dns.GetHostName() });
        }
    }
}
