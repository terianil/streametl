﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Akka.Actor;
using Akka.Configuration;
using Akka.Monitoring;
using Akka.Monitoring.PerformanceCounters;
using TankStationETL.Common.Dtos;

namespace TankStationETL.Node
{
    class Program
    {
        static void Main(string[] args)
        {
            var configText = @"akka {
            actor {
                    provider = ""Akka.Remote.RemoteActorRefProvider, Akka.Remote""
            }

                remote {
                    helios.tcp {
                        port = __portNumber__
                        hostname = __hostname__
		                public-hostname = __publichostname__
                    }
                }
            }";
            configText = configText.Replace("__portNumber__", args[0]);
            configText = configText.Replace("__hostname__", args[1]);
            configText = configText.Replace("__publichostname__", args[2]);

            var config = ConfigurationFactory.ParseString(configText);

            var system = ActorSystem.Create("system", config);

            var monitor = new ActorPerformanceCountersMonitor(new CustomMetrics());
            ActorMonitoringExtension.RegisterMonitor(system, monitor);

            var node = system.ActorOf<Node>("node");

            Console.ReadKey();
        }
    }
}
