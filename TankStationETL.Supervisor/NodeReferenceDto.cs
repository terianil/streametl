using System;
using Akka.Actor;
using Newtonsoft.Json;
using TankStationETL.Common.Dtos;

namespace TankStationETL.Supervisor
{
    public class NodeReferenceDto
    {
        public NodeReferenceDto(IActorRef sender, NodeType type)
        {
            this.Actor = sender;
            Type = type;
        }

        public string Name { get { return this.Actor.Path.ToStringWithAddress(); } }
        public string NodeType { get { return this.Type.ToString(); } }
        [JsonIgnore]
        public IActorRef Actor { get; set; }

        [JsonIgnore]
        public NodeType Type { get; set; }

        public bool Visible { get; set; }

        public override string ToString()
        {
            return String.Format("Node DNS name: {0}, actor path: {1}", this.Actor.Path.Name);
        }
    }
}