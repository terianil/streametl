﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Owin;
using Newtonsoft.Json;
using Owin;

namespace TankStationETL.Supervisor{
    static class OwinExtensions
    {
        public static void Action(this IAppBuilder app, string path, Func<IOwinContext, Task> action)
        {
            app.Use((ctx, next) =>
            {
                if (ctx.Request.Path.Value.Trim('/').Equals(path.Trim('/'), StringComparison.InvariantCultureIgnoreCase))
                {
                    return action(ctx);
                }

                return next();
            });
        }

        public static void File(this IAppBuilder app, string path, string filePath)
        {
            var fullPath = ResolvePath(filePath);

            app.Action(path, ctx =>
            {
                ctx.Response.ContentType = "text/html";
                return ctx.Response.WriteAsync(System.IO.File.ReadAllText(fullPath));
            });            
        }

        public static void Json(this IAppBuilder app, string path, Func<IOwinContext, object> action)
        {
            app.Action(path, ctx =>
            {
                var data = action(ctx);
                var json = JsonConvert.SerializeObject(data);
                ctx.Response.ContentType = "application/json";
                return ctx.Response.WriteAsync(json);
            });
        }

        public static string ResolvePath(string filePath)
        {
            var basePath = Path.GetDirectoryName(typeof (OwinExtensions).Assembly.Location);

#if DEBUG
            basePath = Path.Combine(basePath, "..", "..");
#endif

            var fullPath = Path.Combine(basePath, filePath);
            return fullPath;
        }
    }
}
