﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Akka.Actor;
using Akka.Configuration;

namespace TankStationETL.Supervisor
{
    class Program
    {
        static void Main(string[] args)
        {
            var config = ConfigurationFactory.Load();
            var system = ActorSystem.Create("system", config);
            var etlSupervisor = system.ActorOf<ETLSupervisor>("supervisor");

            Console.ReadKey();
        }
    }
}
