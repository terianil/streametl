﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Web;
using Akka.Actor;
using Microsoft.Owin;
using Microsoft.Owin.FileSystems;
using Microsoft.Owin.Host.HttpListener;
using Microsoft.Owin.Hosting;
using Microsoft.Owin.StaticFiles;
using Owin;
using TankStationETL.Common.Dtos;

namespace TankStationETL.Supervisor
{
    public class ETLSupervisor : TypedActor, IHandle<RegisterNodeDto>, IHandle<CreateNodeDto>
    {
        public List<NodeReferenceDto> Nodes { get; set; }
        private IDisposable web;

        public ETLSupervisor()
        {
            this.Nodes = new List<NodeReferenceDto>();

            StartWebInterface();
        }

        private void StartWebInterface()
        {
            var options = new StartOptions("http://*:81/")
            {
                ServerFactory = typeof(OwinServerFactory).FullName
            };

            Console.WriteLine("Web endpoint: {0}", options.Urls[0]);

            this.web = WebApp.Start(options, BuildWebApp);
        }

        /// <summary>
        /// OWIN AppFunc
        /// </summary>
        /// <param name="root"></param>
        private void BuildWebApp(IAppBuilder root)
        {
            var self = Context.Self;

            root.UseErrorPage();
            root.File("", @"Views\index.html");

            root.Json("/nodes", ctx => this.Nodes);
            root.Json("/emptynodes", ctx => this.Nodes.Where(x=>x.Type == NodeType.None));
            root.Json("/workingnodes", ctx => this.Nodes.Where(x => x.Type != NodeType.None));

            root.File("/create", @"Views\create.html");

            root.Map("/hello", app => app.Run(async ctx =>
            {
                await ctx.Response.WriteAsync("Hello!");
            }));

            root.Json("/newnetwork", ctx =>
            {
                this.Nodes = new List<NodeReferenceDto>();
                return this.Nodes;
            });

            root.Action("createnode", async context =>
            {
                var form = await context.Request.ReadFormAsync();
                var endpoints = form.GetValues("endpoint") ?? new List<string>();
                var node = form.Get("node");
                var nodeTypeString = form.Get("nodetype");

                var nodeType = (NodeType)Enum.Parse(typeof(NodeType), nodeTypeString);
                var command = new CreateNodeDto(node, nodeType, endpoints.ToList());
                self.Tell(command);

                this.Nodes.Single(x => x.Name == node).Type = nodeType;

                context.Response.Redirect(@"/");
            });

            root.UseStaticFiles(new StaticFileOptions
            {
                RequestPath = new PathString("/scripts"),
                FileSystem = new PhysicalFileSystem(OwinExtensions.ResolvePath("scripts"))
            });
        }

        public void OnStop()
        {
            this.web.Dispose();
        }

        public void Handle(RegisterNodeDto message)
        {
            this.Nodes.Add(new NodeReferenceDto(Sender, NodeType.None));
        }

        public void Handle(CreateNodeDto message)
        {
            var target = Context.ActorSelection(message.Target);
            target.Tell(message);
        }
    }
}