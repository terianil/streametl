﻿function Item() {
    this.value = ko.observable('');
};

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function ViewModel() {
    var curr = this;
    curr.emptyNodes = ko.observableArray([]);
    curr.workingNodes = ko.observableArray([]);
    curr.endpoints = ko.observableArray([]);
    curr.selectedNode = ko.observable('');

    curr.addEndpoint = function () {       
        curr.endpoints.push(new Item());
        return false;
    };

    curr.removeEndpoint = function(item) {
        curr.endpoints.remove(item);
    };

    curr.refresh = function () {
        $.getJSON("/emptynodes", function (data) {
            curr.emptyNodes(data);
            curr.selectedNode(getParameterByName("node"));
        });

        $.getJSON("/workingnodes", function (data) {
            curr.workingNodes(data);
        });
    };
}

window.viewModel = new ViewModel();
window.viewModel.refresh();
ko.applyBindings(window.viewModel);