﻿namespace TankStationETL.Aggregation
{
    /// <summary>
    /// Representation of aggregation unit, which contains first and last tuple in time window.
    /// </summary>
    /// <typeparam name="T">
    ///     Genertic tuple.
    /// </typeparam>
    class BoundaryElementsContainer<T> where T : class
    {
        public T First { get; private set; }
        public T Last { get; private set; }

        /// <summary>
        /// Returs true when First and Last elements are not null
        /// </summary>
        public bool IsBounded
        {
            get { return First != null && Last != null; }
        }

        /// <summary>
        /// Adds element to time window.
        /// </summary>
        /// <param name="element"></param>
        public void Add(T element)
        {
            if (First == null)
                First = element;
            else
            {
                Last = element;
            }
        }

        /// <summary>
        /// Clears time windows. Next window would be continous based on Last element from previous.
        /// </summary>
        public void Clear()
        {
            if (IsBounded)
            {
                First = Last;
            }
            else
            {
                First = null;
            }

            Last = null;
        }
    }
}
