namespace TankStationETL.DTO
{
    public class CreateEtlsRequest
    {
        public int NumerOfEtls { get; set; }
    }
}