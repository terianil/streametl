using System;

namespace TankStationETL.DTO
{
    [Serializable]
    public class MessageDto
    {
        public object Payload { get; private set; }

        public MessageDto(object payload)
        {
            this.Payload = payload;
        }
    }
}