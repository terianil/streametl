using System;
using System.ComponentModel.DataAnnotations;

namespace TankStationETL.DTO
{
    /// <summary>
    /// Nozzle message representation.
    /// </summary>
    [Serializable]
    public class NozzleMessageDto
    {
        public int TankId { get; set; }
        public int NozzleId { get; set; }
        public DateTime Timestamp { get; set; }
        public double CalibrationFactor { get; set; }

        [Range(0, int.MaxValue)]
        public double TotalVolumePumped { get; set; }
        public string SendTime { get; set; }
    }
}