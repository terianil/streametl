namespace TankStationETL.DTO
{
    public class NozzleOutputMessageDto
    {
        public object OutputMessage { get; set; }

        public NozzleOutputMessageDto(object outputMessage)
        {
            OutputMessage = outputMessage;
        }
    }
}