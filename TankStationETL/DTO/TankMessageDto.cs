using System;

namespace TankStationETL.DTO
{
    /// <summary>
    /// Tank message representation.
    /// </summary>
    [Serializable]
    public class TankMessageDto
    {
        public int TankId { get; set; }
        public DateTime Timestamp { get; set; }
        public double VolumeBrutto { get; set; }
        public double WaterVolume { get; set; }
        public double Temperatue { get; set; }
        public string SendTime { get; set; }
    }
}