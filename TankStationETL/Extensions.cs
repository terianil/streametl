using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace TankStationETL
{
    static internal class Extensions
    {
        public static JObject ReadAsJObject(this Stream stream)
        {
            using (var streamReader = new StreamReader(stream))
            {
                using (var jsonReader = new JsonTextReader(streamReader))
                {
                    return JObject.Load(jsonReader);
                }
            }
        }
        public static string ReadAsString(this Stream stream)
        {
            using (var streamReader = new StreamReader(stream))
            {
                return streamReader.ReadToEnd();
            }
        }
    }
}