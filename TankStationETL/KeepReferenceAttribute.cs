﻿using System;

namespace TankStationETL
{
    /// <summary>
    /// Helper attribute to prevent compiler from optimizing references
    /// </summary>
    [AttributeUsage(AttributeTargets.Assembly, AllowMultiple = true)]
    public class KeepReferenceAttribute : Attribute
    {
        public Type T { get; set; }

        public KeepReferenceAttribute(Type t)
        {
            this.T = t;
        }
    }
}