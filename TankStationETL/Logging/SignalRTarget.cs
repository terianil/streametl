﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR.Client;
using NLog;
using NLog.Targets;

namespace TankStationETL.Logging
{
    /// <summary>
    /// NLog target that uses SignalR hub
    /// </summary>
    class SignalRTarget : Target
    {
        private HubConnection connection;
        private IHubProxy hub;
        
        public string HubsUri { get; private set; }
        public string RoleName { get; private set; }

        public SignalRTarget(string hubsUri, string roleName)
        {
            this.HubsUri = hubsUri;
            this.RoleName = roleName;
        }

        protected override void InitializeTarget()
        {
            this.connection = new HubConnection(this.HubsUri);
            this.connection.TransportConnectTimeout = TimeSpan.FromSeconds(1);
            this.hub = this.connection.CreateHubProxy("LogHub");            

        }

        protected override async void Write(LogEventInfo logEvent)
        {
            if (this.connection.State != ConnectionState.Connected)
            {
                await this.connection.Start();
            }

            await this.hub.Invoke("Log", this.RoleName, logEvent.Level.Name, logEvent.LoggerName, logEvent.TimeStamp, logEvent.FormattedMessage);

        }
    }
}
