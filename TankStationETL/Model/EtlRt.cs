using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Threading;
using Akka.Actor;
using Newtonsoft.Json;
using NLog;
using TankStationETL.Aggregation;
using TankStationETL.Common.Dtos;
using TankStationETL.DTO;
using TankStationETL.ModelBase;

namespace TankStationETL.Model
{
    /// <summary>
    /// Tank Station Etl process.
    /// </summary>
    public class EtlRt : EtlBase, IHandle<TankMessageDto>, IHandle<NozzleMessageDto>, IHandle<NozzleOutputMessageDto>
    {
        private static readonly Logger Log = LogManager.GetLogger("ETL");
        private Dictionary<string, BoundaryElementsContainer<NozzleStatus>> aggregatedNozzles;
        private Timer nozzleAggregationTimer;
        private BoundaryElementsContainer<NozzleStatus> boundaryNozzleMessages;
        private IActorRef selfRef;
        public bool IsNew { get; set; }

        public EtlRt()
        {
            this.IsNew = true;
            Initialize();
        }

        protected void Initialize()
        {
            this.nozzleAggregationTimer = new Timer(AggregateNozzleStatus, null, TimeSpan.FromSeconds(20), TimeSpan.FromSeconds(20));
            this.aggregatedNozzles = new Dictionary<string, BoundaryElementsContainer<NozzleStatus>>();
        }

        public override void AroundPreStart()
        {
            this.selfRef = Context.Self;
        }

        /// <summary>
        /// Implementation of aggregation tuples for each Nozzle.
        /// Transformation from raw data to computed values.
        /// </summary>
        /// <param name="state">Current aggregation state.</param>
        private void AggregateNozzleStatus(object state)
        {
            foreach (var nozzle in aggregatedNozzles)
            {
                var nozzleStatus = nozzle.Value;
                if (nozzleStatus.IsBounded)
                {
                    var rawVolume = nozzleStatus.Last.TotalVolumePumped - nozzleStatus.First.TotalVolumePumped;
                    var brutto = rawVolume * nozzleStatus.First.CalibrationFactor;
                    var netto = brutto * 0.9;

                    var outputMessage = new
                    {
                        TankId = nozzleStatus.First.TankId,
                        NozzleId = nozzleStatus.First.NozzleId,
                        BeginTimestamp = nozzleStatus.First.Timestamp,
                        EndTimestamp = nozzleStatus.Last.Timestamp,
                        Interval = nozzleStatus.Last.Timestamp - nozzleStatus.First.Timestamp,
                        RawVolume = rawVolume,
                        BruttoVolume = brutto,
                        NettoVolume = netto
                    };

                    //Out("Nozzles", outputMessage);
                    selfRef.Tell(new NozzleOutputMessageDto(outputMessage));
                }
                else
                {
                    Log.Warn("Nozzle {0} is not bounded", nozzle.Key);
                }

                nozzleStatus.Clear();
            }

            Log.Info("Aggregating nozzle status");
        }

        /// <summary>
        /// Validate Nozzle messages and initiate aggregation process in current time window.
        /// </summary>
        /// <param name="msg"></param>
        private void ProcessNozzleMessage(NozzleMessageDto msg)
        {
            if (!IsValid(msg))
            {
                Log.Error("Invalid message: {0}", JsonConvert.SerializeObject(msg));
                return;
            }

            var key = string.Format("{0}/{1}", msg.TankId, msg.NozzleId);

            if (!this.aggregatedNozzles.ContainsKey(key))
            {
                this.aggregatedNozzles[key] = new BoundaryElementsContainer<NozzleStatus>();
            }

            var nozzleStatus = this.aggregatedNozzles[key].Last ?? this.aggregatedNozzles[key].First;

            if (nozzleStatus != null && nozzleStatus.Timestamp > msg.Timestamp)
            {
                Log.Warn("Out of order message: {0}", JsonConvert.SerializeObject(msg));

                return;
            }

            this.aggregatedNozzles[key].Add(NozzleStatus.Dump(msg));
        }

        /// <summary>
        /// Simple processing TankMessages without aggregations.
        /// </summary>
        /// <param name="msg"></param>
        private void ProcessTankMessage(TankMessageDto msg)
        {
            if (!IsValid(msg))
            {
                Log.Error("Invalid message: {0}", JsonConvert.SerializeObject(msg));
                return;
            }

            var outputMessage = new
            {
                TankId = msg.TankId.ToString(),
                Timestamp = msg.Timestamp,
                VolumeBrutto = msg.VolumeBrutto,
                VolumeNetto = CalculateTankVolumeNetto(msg),
                WaterVolume = msg.WaterVolume,
                Temperature = msg.Temperatue,
                SendTime = msg.SendTime,
            };

            Out("Tanks", outputMessage);
        }

        private double CalculateTankVolumeNetto(TankMessageDto msg)
        {
            return (msg.VolumeBrutto - msg.WaterVolume) * 0.9;
        }

        private bool IsValid<T>(T message)
        {
            var context = new ValidationContext(message);
            var results = new List<ValidationResult>();

            return Validator.TryValidateObject(message, context, results, true);
        }


        public void Handle(TankMessageDto messageDto)
        {
            ProcessTankMessage(messageDto);

            if (this.IsNew)
            {
                Sender.Tell(new RequestHistoricalDataDto());
                this.IsNew = false;
            }
        }

        public void Handle(NozzleMessageDto messageDto)
        {
            ProcessNozzleMessage(messageDto);
        }

        public void Handle(NozzleOutputMessageDto message)
        {
            this.Out("Nozzles", message.OutputMessage);
        }
    }
}