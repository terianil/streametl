using System;
using System.Text;
using Akka.Actor;
using MongoDB.Bson;
using MongoDB.Driver;
using NLog;
using TankStationETL.DTO;
using TankStationETL.ModelBase;
using System.Configuration;
using Newtonsoft.Json;

namespace TankStationETL.Model
{
    /// <summary>
    /// FTI that uses MongoDB to store documents
    /// </summary>
    public class FtiMongoDb : FtiBase, IHandle<FtiInputDto>
    {
        private static readonly Logger Log = LogManager.GetLogger("FTI");

        /// <summary>
        /// Stores document in collection which name is stored in headers and content is body of message
        /// </summary>
        /// <param name="properties">Properties of message</param>
        /// <param name="message">Message body</param>
        public void Handle(FtiInputDto message)
        {
            var connectionString = ConfigurationManager.AppSettings.Get("MongoDb");
            var client = new MongoClient(connectionString);
            var server = client.GetServer();
            var database = server.GetDatabase("tpdia");

            Log.Debug("Adding message to collection {0}", message.Collection);

            var collection = database.GetCollection(message.Collection);

            try
            {
                dynamic json = JsonConvert.DeserializeObject(message.Data);
                json.ReceiveTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss,fff");
                var bson = BsonDocument.Parse(JsonConvert.SerializeObject(json));
                collection.Insert(bson);
            }
            catch (Exception e)
            {
                Log.Error("Error inserting document", e);
            }
            
            server.Disconnect();
        }
    }
}
