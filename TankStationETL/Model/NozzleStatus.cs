using System;
using TankStationETL.DTO;

namespace TankStationETL.Model
{
    /// <summary>
    /// Nozzle status class.
    /// </summary>
    internal class NozzleStatus
    {
        public double TotalVolumePumped { get; set; }

        public DateTime Timestamp { get; set; }

        public int TankId { get; set; }

        public int NozzleId { get; set; }

        public double CalibrationFactor { get; set; }

        /// <summary>
        /// Converts NozzleMessage to NozzleStatus.
        /// </summary>
        /// <param name="msg">NozzleMessage.</param>
        /// <returns>NozzleStatus.</returns>
        public static NozzleStatus Dump(NozzleMessageDto msg)
        {
            return new NozzleStatus
            {
                Timestamp = msg.Timestamp,
                TotalVolumePumped = msg.TotalVolumePumped,
                TankId = msg.TankId,
                NozzleId = msg.NozzleId,
                CalibrationFactor = msg.CalibrationFactor
            };
        }
    }
}