﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Akka.Actor;
using Akka.Monitoring;
using Microsoft.Owin.Diagnostics;
using Microsoft.Owin.Host.HttpListener;
using Microsoft.Owin.Hosting;
using Newtonsoft.Json;
using NLog;
using Owin;
using TankStationETL.Common.Dtos;
using TankStationETL.DTO;
using TankStationETL.ModelBase;

namespace TankStationETL.Model
{
    /// <summary>
    /// RBF node for tank messages
    /// </summary>
    public class Rbf : EndpointManagementActorBase, IHandle<MessageDto>, IHandle<RequestHistoricalDataDto>
    {
        private IDisposable web;

        private static readonly Logger Log = LogManager.GetLogger("Rbf");
        public FixedSizedQueue<MessageDto> HistoricalMessageQueue { get; private set; }
        public bool Fail { get; set; }

        public Rbf()
        {
            this.HistoricalMessageQueue = new FixedSizedQueue<MessageDto>(RoleEnvironment.HistoricalMessageQueueLength);
            this.Fail = false;
            Run();
        }

        public void Run()
        {
            NLogConfig.ConfigureLogging();

            StartWebInterface();
        }

        /// <summary>
        /// Exposes HTTP interface that is used to receive messages
        /// </summary>
        private void StartWebInterface()
        {
            var options = new StartOptions(string.Format("http://{0}:20000/{1}", RoleEnvironment.RBFEndpoint, this.Self.Path.Name))
            {
                ServerFactory = typeof(OwinServerFactory).FullName
            };

            Console.WriteLine("Web endpoint: {0}", options.Urls[0]);
            Trace.TraceInformation("Web endpoint: {0}", options.Urls[0]);

            this.web = WebApp.Start(options, BuildWebApp);
        }

        /// <summary>
        /// OWIN AppFunc
        /// </summary>
        /// <param name="root"></param>
        private void BuildWebApp(IAppBuilder root)
        {
            root.UseErrorPage(new ErrorPageOptions() { ShowExceptionDetails = true });

            var s = this.Self;

            root.Map("/input/nozzle/input", app => app.Run(async ctx =>
            {
                var incomingMessage = ctx.Request.Body.ReadAsString();

                var nozzleMessage = JsonConvert.DeserializeObject<NozzleMessageDto>(incomingMessage);

                if (nozzleMessage != null)
                    s.Tell(new MessageDto(nozzleMessage));

                await ctx.Response.WriteAsync("ACK");
            }));

            root.Map("/input/tank/input", app => app.Run(async ctx =>
            {
                var incomingMessage = ctx.Request.Body.ReadAsString();

                var tankMessage = JsonConvert.DeserializeObject<TankMessageDto>(incomingMessage);

                if (tankMessage != null)
                    s.Tell(new MessageDto(tankMessage));

                await ctx.Response.WriteAsync("ACK");
            }));
        }

        /// <summary>
        /// Handles message that is incoming via HTTP interface
        /// </summary>
        /// <param name="message"></param>

        public void OnStop()
        {
            this.web.Dispose();
        }

        public void Handle(MessageDto message)
        {
            Context.IncrementMessagesReceived();

            this.HistoricalMessageQueue.Enqueue(message);

            foreach (var endpoint in this.Endpoints)
            {
                var selected = Context.System.ActorSelection(endpoint + "/rif");
                selected.Tell(message);
            }
        }

        public void Handle(RequestHistoricalDataDto message)
        {
            foreach (var messageDto in this.HistoricalMessageQueue)
            {
                Sender.Tell(messageDto);
            }
        }
    }
}