using System.Collections.Generic;
using System.Linq;
using System.Text;
using Akka.Actor;
using Akka.Monitoring;
using NLog;
using TankStationETL.Common.Dtos;
using TankStationETL.DTO;
using TankStationETL.ModelBase;

namespace TankStationETL.Model
{
    /// <summary>
    /// RIF node for Tanks
    /// </summary>
    public class Rif : EndpointManagementActorBase, IHandle<MessageDto>, IHandle<RequestHistoricalDataDto>
    {
        private static readonly Logger Log = LogManager.GetLogger("Rif");
        public FixedSizedQueue<MessageDto> HistoricalMessageQueue { get; private set; }
        public bool Fail { get; set; }

        public Rif()
        {
            this.IsNew = true;
            this.HistoricalMessageQueue = new FixedSizedQueue<MessageDto>(RoleEnvironment.HistoricalMessageQueueLength);
            NLogConfig.ConfigureLogging();
        }

        public bool IsNew { get; set; }

        /// <summary>
        /// Handles message received from queue
        /// </summary>
        /// <param name="properties">Message properties</param>
        /// <param name="body">Message body</param>
        /// <summary>
        /// Handles message received from queue
        /// </summary>
        /// <param name="properties">Message properties</param>
        /// <param name="body">Message body</param>
        public void Handle(MessageDto message)
        {
            Context.IncrementMessagesReceived();

            this.HistoricalMessageQueue.Enqueue(message);
            this.HistoricalMessageQueue.Store(message);

            foreach (var endpoint in this.Endpoints)
            {
                var selected = Context.System.ActorSelection(endpoint + "/etl");
                selected.Tell(message.Payload);
            }

            if (this.IsNew)
            {
                Sender.Tell(new RequestHistoricalDataDto());
                this.IsNew = false;
            }
        }

        public void Handle(RequestHistoricalDataDto message)
        {
            foreach (var messageDto in this.HistoricalMessageQueue)
            {
                Sender.Tell(messageDto);
            }
        }
    }
}