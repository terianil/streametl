﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Akka.Actor;
using TankStationETL.Common.Dtos;

namespace TankStationETL.ModelBase
{
    public class EndpointManagementActorBase : TypedActor, IHandle<SetEndpointsDto>
    {
        public List<string> Endpoints { get; set; }

        public void Handle(SetEndpointsDto message)
        {
            this.Endpoints = message.EndpointList;
        }
    }
}
