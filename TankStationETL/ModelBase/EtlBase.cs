﻿using System.Collections.Generic;
using System.Text;
using System.Threading;
using Akka.Actor;
using Newtonsoft.Json;
using TankStationETL.DTO;

namespace TankStationETL.ModelBase
{
    /// <summary>
    /// Base class for ETL processes.
    /// </summary>
    public abstract class EtlBase : EndpointManagementActorBase
    {
        public void Run()
        {
            NLogConfig.ConfigureLogging();
        }

        /// <summary>
        /// Outputs one message to be stored in given collection
        /// </summary>
        /// <param name="collection">Collection name</param>
        /// <param name="outputMessage">Message to store</param>
        protected void Out(string collection, object outputMessage)
        {
            var json = JsonConvert.SerializeObject(outputMessage);

            var toFti = new FtiInputDto() {Collection = collection, Data = json};

            foreach (var endpoint in this.Endpoints)
            {
                var selected = Context.System.ActorSelection(endpoint + "/fti");
                selected.Tell(toFti);
            }
        }
    }
}
