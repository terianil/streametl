﻿using System.Threading;
using Akka.Actor;
using TankStationETL.DTO;

namespace TankStationETL.ModelBase
{
    /// <summary>
    /// Base class for FTI node
    /// </summary>
    public abstract class FtiBase : EndpointManagementActorBase
    {
        public void Run()
        {
            NLogConfig.ConfigureLogging();
        }
    }
}
