﻿using NLog;
using NLog.Config;
using NLog.Targets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TankStationETL.Logging;

namespace TankStationETL
{
    /// <summary>
    /// Helper class for configuring NLog in worker process
    /// </summary>
    public static class NLogConfig
    {
        public static void ConfigureLogging()
        {
            var logHubUri = RoleEnvironment.LogUri;

            var traceTarget = new TraceTarget();
            var signalRTarget = new SignalRTarget(logHubUri, RoleEnvironment.RoleName);
            var console = new ColoredConsoleTarget();

            var config = new LoggingConfiguration();

            config.AddTarget("trace", traceTarget);
            //config.LoggingRules.Add(new LoggingRule("*", LogLevel.Trace, traceTarget));
            //config.LoggingRules.Add(new LoggingRule("*", LogLevel.Trace, signalRTarget));
            config.LoggingRules.Add(new LoggingRule("*", LogLevel.Trace, console));

            LogManager.Configuration = config;
            LogManager.ReconfigExistingLoggers();
        }
    }
}
