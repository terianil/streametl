﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;

namespace TankStationETL {
    public static class RoleEnvironment {
        public static string RoleName { get; set; }
        public static string LogUri { get; set; }
        public static string MongoDb { get { return "mongodb://localhost"; } }
        public static string RBFEndpoint
        {
            get { return "*"; } 
        }

        public static int HistoricalMessageQueueLength { get { return 100; } }
    }
}
