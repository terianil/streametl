﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Akka.Actor;
using Akka.Routing;
using TankStationETL.DTO;
using TankStationETL.Model;

namespace TankStationETL.Supervisors {
    public class EtlSupervisor : TypedActor, IHandle<CreateEtlsRequest> {
        public void Handle(CreateEtlsRequest message) {
            var routees = Enumerable
                .Range(1, message.NumerOfEtls)
                .Select(i => new ActorRefRoutee(Context.ActorOf<EtlRt>("etl" + i)))
                .ToArray();
        }
    }
}
