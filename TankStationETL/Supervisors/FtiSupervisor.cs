﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Akka.Actor;
using Akka.Routing;
using TankStationETL.DTO;
using TankStationETL.Model;

namespace TankStationETL.Supervisors {
    public class FtiSupervisor : TypedActor, IHandle<CreateFtisRequest> {
        public void Handle(CreateFtisRequest message) {
            var routees = Enumerable
                .Range(1, message.NumerOfFtis)
                .Select(i => new ActorRefRoutee(Context.ActorOf<FtiMongoDb>("fti" + i)))
                .ToArray();
        }
    }
}
