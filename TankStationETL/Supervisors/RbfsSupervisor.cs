﻿using System.Linq;
using Akka.Actor;
using Akka.Routing;
using TankStationETL.DTO;
using TankStationETL.Model;

namespace TankStationETL.Supervisors
{
    public class RbfsSupervisor : TypedActor, IHandle<CreateRbfsRequest> {
        public void Handle(CreateRbfsRequest message) {
            var routees = Enumerable
                .Range(1, message.NumerOfRbfs)
                .Select(i => new ActorRefRoutee(Context.ActorOf<Rbf>("rbf" + i)))
                .ToArray();
        }
    }
}