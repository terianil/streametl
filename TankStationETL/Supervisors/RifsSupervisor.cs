using System.Linq;
using Akka.Actor;
using Akka.Routing;
using TankStationETL.DTO;
using TankStationETL.Model;

namespace TankStationETL.Supervisors
{
    public class RifsSupervisor : TypedActor, IHandle<CreateRifsRequest>
    {
        public void Handle(CreateRifsRequest message)
        {
            var routees = Enumerable
                .Range(1, message.NumerOfRifs)
                .Select(i => new ActorRefRoutee(Context.ActorOf<Rif>("rif" + i)))
                .ToArray();
        }
    }
}