﻿$url = 'http://localhost:9999/generator'

for($tank = 0; $tank -le 3; $tank++)
{
    for($nozzle = 0; $nozzle -le 3; $nozzle++)
    {
        1..4 | % {
            Invoke-RestMethod "$url/tank/$tank/nozzle/$nozzle/pour?volume=30" -Method Post | Out-Null
        }
    }
}